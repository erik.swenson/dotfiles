#!/bin/bash

# Ensure pip has been installed and updated.
python -m ensurepip --upgrade
pip3 install --upgrade pip

# Install virtualenvwrapper utility
pip3 install virtualenvwrapper
source $HOME/.local/bin/virtualenvwrapper.sh

# Verify that the install worked.
python3.7 -V
verify=$?
if [[ "0" == $verify ]]; then
    printf "Python 3.7 install succeeded!"
else
    printf "Python 3.7 install FAILED!"
fi

# Create a virtual environment called "panel_proxies" where you can build and test the panel code.
CWD=`pwd`
EMBEDDED_APPS_DIR=$HOME/git/embedded-apps
cd $EMBEDDED_APPS_DIR
# You can replace 'panel_proxies' with whatever name you want for your environment.
mkvirtualenv -p python3.7 -r requirements.txt --setuptools 57.4.0 panel_proxies
cd $CWD


