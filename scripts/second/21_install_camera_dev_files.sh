#!/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
DEV_FILE_DIR=$SCRIPT_DIR/../../camera/developer
AUTH_KEYS_FILE=$DEV_FILE_DIR/rootfs/etc/ssh/authorized_keys
CAMERA_BUILD_DIR=$HOME/git/camera-build

rm -f $AUTH_KEYS_FILE
touch -f $AUTH_KEYS_FILE
cat ~/.ssh/id_ed25519.pub >> $AUTH_KEYS_FILE
cat ~/.ssh/id_rsa.pub >> $AUTH_KEYS_FILE
chmod 664 $AUTH_KEYS_FILE
cp -r $DEV_FILE_DIR $CAMERA_BUILD_DIR/.developer
