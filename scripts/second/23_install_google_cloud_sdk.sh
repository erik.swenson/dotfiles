#!/bin/bash

attempts=0
while [ "$attempts" -lt "3" ]; do
    ((attempts=attempts + 1))
    printf "\n\nAttempting to install Google Cloud SDK, attempt $attempts\n\n"

    # See if `gcloud` has been installed.
    which gcloud
    result=$?
    if [ "$result" -ne "0" ]; then
        # If we can't find it, try to install again.
        sudo snap install google-cloud-sdk --classic
        sleep 3
    fi
done

# Now init the GCP stuff.
gcloud init
