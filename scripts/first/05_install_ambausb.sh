#!/bin/bash

printf "\n\nBegin AmbaUSB install\n\n"

START_DIR=`pwd`

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
SOFTWARE_DIR=$SCRIPT_DIR/../../software
TMP_DIR=$SOFTWARE_DIR/tmp

mkdir $TMP_DIR
cd $TMP_DIR
file_name=`find ../ -name *AmbaUSB* | xargs basename`
printf "Found AmbaUSB archive: %s\n" $file_name

printf "Installing AmbaUSB RPM packages\n"
unzip ../$file_name -d $TMP_DIR/

sudo rpm -ih */Fedora/rpms/*.rpm

cd $START_DIR
rm -rf $TMP_DIR
printf "\n\nDone installing AmbaUSB\n\n"
