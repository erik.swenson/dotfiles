#!/bin/bash

# To enable the free repository.
sudo dnf install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm -y

# To enable the non-free repository.
sudo dnf install https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm -y

# Install the spotify client.
sudo dnf install lpf-spotify-client -y

# Add the user to the pkg-build group. After a reboot, they'll then be able to build and run the spotify package.
sudo usermod -aG pkg-build $USER