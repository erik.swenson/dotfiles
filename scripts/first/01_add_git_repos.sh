#!/bin/bash

git_dir=$HOME/git
[ -d $git_dir ] || mkdir $git_dir

camera_build=$git_dir/camera-build
if ! [ -d $camera_build ]; then
  printf "\n\nCloning camera-build repo from GitLab remote . . .\n\n" 
  git clone --recursive git@gitlab.com:vivint/camera/camera-build.git $camera_build
  printf "\n\nDone.\n\n"
else
  printf "\n\ncamera-build repo already found!\n\n"
fi

embedded_apps=$git_dir/embedded-apps
if ! [ -d $embedded_apps ]; then
  printf "\n\nCloning embedded-apps repo from BitBucket remote . . .\n\n" 
  git clone --recursive ssh://git@source.vivint.com:7999/em/embedded-apps.git $embedded_apps
  printf "\n\nDone.\n\n"
else
  printf "\n\nembedded-apps repo already found!\n\n"
fi
