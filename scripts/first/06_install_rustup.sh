#!/bin/bash

# Download and execute the rust installation script.
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh

# Get all of the rust environment in your current shell.
source $HOME/.cargo/env
