#!/bin/bash

# General - vim, wget
# Rust - clang, cmake, curl, dbus-devel, dnf-plugins-core, gcc, make, pkgconf-pkg-config
# AmbaUSB - qt5-qtmultimedia
# Docker - docker-compose, moby-engine
# Snap - snapd
# Panel Virtual Environment - alsa-lib-devel, bzip2-devel, libffi-devel, ncurses-devel, openssl-devel, python37,
#                             readline-devel, redhat-rpm-config, sqlite-devel, xz-devel, zlib-devel, zlibrary
# We probably don't need all of these ^^^ (for panel venv) but I don't know which to take out yet.
sudo dnf install \
    vim \
    wget \
    clang \
    cmake \
    curl \
    dbus-devel \
    dnf-plugins-core \
    gcc \
    make \
    pkgconf-pkg-config \
    qt5-qtmultimedia \
    docker-compose \
    moby-engine \
    snapd \
    dnf-plugins-core \
    alsa-lib-devel \
    bzip2-devel \
    libffi-devel \
    ncurses-devel \
    openssl-devel \
    python37 \
    readline-devel \
    redhat-rpm-config \
    sqlite-devel \
    xz-devel \
    zlib-devel \
    zlibrary \
    -y
