#!/bin/bash

slack_rpm=slack-4.21.1-0.1.fc21.x86_64.rpm
slack_rpm_file=/tmp/$slack_rpm
wget -O $slack_rpm_file https://downloads.slack-edge.com/releases/linux/4.21.1/prod/x64/$slack_rpm
sudo dnf install $slack_rpm_file -y