#!/bin/bash
sudo dnf install moby-engine docker-compose -y
sudo systemctl enable docker
sudo groupadd docker
sudo usermod -aG docker $USER
