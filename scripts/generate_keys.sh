#!/bin/bash

# Create the ssh directory
ssh_dir=$HOME/.ssh
[ -d "$ssh_dir" ] || mkdir $ssh_dir
chmod 700 $ssh_dir

# Generate an ed25519 key.
ssh_dir=$HOME/.ssh
if ! [ -f $ssh_dir/id_ed25519 ]; then
  printf "\n\nGenerating an ed25519 public/private key pair.\n\n"
  ssh-keygen -t ed25519 -f $ssh_dir/id_ed25519 -C "$USER@vivint.com"
fi

# Generate an ed25519 key to use for tigerguard.
if ! [ -f $ssh_dir/tigerguard_ed25519 ]; then
  printf "\n\nGenerating an ed25519 public/private key pair for tigerguard.\n"
  printf "!!!!!!!!!! You should probably choose a good password for this one (don't leave it empty) !!!!!!!!!!\n\n"
  ssh-keygen -t ed25519 -f $ssh_dir/tigerguard_ed25519 -C "$USER@vivint.com"
fi

# Generate an RSA key.
if ! [ -f $ssh_dir/id_rsa ]; then
  printf "\n\nGenerating an RSA public/private key pair.\n\n"
  ssh-keygen -f $ssh_dir/id_rsa -C "$USER@vivint.com"
fi

# Ensure proper permissions.
chmod 600 $ssh_dir/id_rsa
chmod 600 $ssh_dir/id_ed25519
chmod 600 $ssh_dir/tigerguard_ed25519
chmod 644 $ssh_dir/id_rsa.pub
chmod 644 $ssh_dir/id_ed25519.pub
chmod 644 $ssh_dir/tigerguard_ed25519.pub

