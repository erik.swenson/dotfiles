#/bin/bash

HD_PATH=$(lsblk | grep sda2 | awk '{print $7}')
SYNC_DEST=$HD_PATH/home_sync
SYNC_SRC=/home
LOG_FILE=/tmp/home_sync.log

/usr/bin/date &> $LOG_FILE
echo "HD_PATH = $HD_PATH" &> $LOG_FILE
echo "SYNC_DEST = $SYNC_DEST" &> $LOG_FILE
echo "SYNC_SRC = $SYNC_SRC" &> $LOG_FILE

/usr/bin/rsync -a --delete $SYNC_SRC $SYNC_DEST &> $LOG_FILE
