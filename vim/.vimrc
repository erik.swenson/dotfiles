" Use syntax highlighting
syntax on

" Set the color scheme
colorscheme slate

" Show relative line numbers, but also current line number.
set relativenumber
set number

" Highlight current line
set cursorline

" Use two spaces for tabs
set expandtab
set tabstop=2

" Highlight column 80
set colorcolumn=80
highlight ColorColumn ctermbg=black
highlight ColorColumn guibg=black

" Remap keys
imap jj <ESC>

