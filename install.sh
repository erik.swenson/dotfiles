#!/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

# Run all the installation scripts.
for f in $SCRIPT_DIR/scripts/first/*; do
  /bin/sh $f
done

printf "\n\nDone with initial installation. Reboot your computer and complete the installation by running:\n\n"
printf "\tcd $SCRIPT_DIR; ./finalize.sh\n\n"
