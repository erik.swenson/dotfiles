#!/bin/bash

if [ -f $HOME/.local/bin/virtualenvwrapper.sh ]; then
    source $HOME/.local/bin/virtualenvwrapper.sh
fi

if [ -f $HOME/.cameras ]; then
  source $HOME/.cameras
fi

export EDITOR=/usr/bin/vim # instead of nano
export GOOGLE_APPLICATION_CREDENTIALS=$HOME/data-pipeline-323223-eea8b31b214a.json
export LABELBOX_API_KEY="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiJjbDJxY2VjOXAwN210MDdjNzIwZmk4Zm5zIiwib3JnYW5pemF0aW9uSWQiOiJjbDJxY2VjOWIwN21zMDdjNzZsaWhmYWI0IiwiYXBpS2V5SWQiOiJjbDJ1dWN5d2Exc2kyMDdjZTN6N3g1eDRhIiwic2VjcmV0IjoiN2ZiZmE2YTgxMDQ2Nzc5ODAwMzUwMzdkZDE2OTQ1NjYiLCJpYXQiOjE2NTE4NjU5ODksImV4cCI6MjI4MzAxNzk4OX0.BqY80e4PEOsbcCHAjVtoa-xhIRDSKglIVG41viqmJHE"

