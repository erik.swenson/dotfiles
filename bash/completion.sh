#!/bin/bash

if [ -f /etc/profile.d/bash_completion.sh ]; then
  source /etc/profile.d/bash_completion.sh
fi

if [ -f $HOME/.git-completion.bash ]; then
  . $HOME/.git-completion.bash
fi

if [ -f $HOME/.cameras ]; then
  . $HOME/.cameras

  _camera_name_complete() {
    local cur=${COMP_WORDS[$COMP_CWORD]}
    case $COMP_CWORD in
      1)
        COMPREPLY=( $(compgen -W '$FAKE_COATI_NAMES $HOME_CAMERA_NAMES $DEV_CAMERA_NAMES $EMPLOYEE_CAMERA_NAMES $COLUMBUS_CAMERA_NAMES $HORNET_SWARM_CAMERA_NAMES $SED_TESTER_CAMERA_NAMES $CHAMBER_CAMERA_NAMES $JUSTIN_CAMERA_NAMES $CHRIS_CAMERA_NAMES' -o default -- $cur) )
        ;;
      *)
        COMPREPLY=( $(compgen -o default -- $cur) )
        ;;
    esac
  }
  
  complete -F _camera_name_complete get_uuid_from_name
  complete -F _camera_name_complete tg
  complete -F _camera_name_complete tgcmd
  complete -F _camera_name_complete tgcp
  complete -F _camera_name_complete device_config
  complete -F _camera_name_complete device_state
  complete -F _camera_name_complete embeddings

fi

