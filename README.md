# Dotfiles

This repo provides an easy way to get set up for camera development on a new machine.

## Access

Before you will be able to do anything you need access to a few resources:

- GitLab (for camera development)
- Bitbucket (for panel development)
- Google Workspace Account (for lots of things)

## Installation

Currently, this repo provides support for Fedora 35 and testing has only been performed against that OS.

### Operating System

TODO

### Development Environment

After you install your Fedora OS (see the "Operating System" section above for details), follow these steps to get your
machine ready for camera development.

First, we have to do a couple things manually so that the install scripts run smoothly:

1. Clone this repo: `git clone --recursive https://gitlab.com/erik.swenson/dotfiles.git ~/.dotfiles`
1. `cd ~/.dotfiles`
1. There are a couple of scripts you need to run separately, one to generate some SSH keys and one to set up your
connection to the VPN.
1. `./scripts/setup_vpn.sh`
1. You need to be on the company network in order to access anything related to Atlassian tools (Bitbucket, JIRA, 
Confluence) and to download some of the files in this installation. If you are not on the company network (i.e. not in 
the office), you need to connect to the vpn by running:
`sudo openconnect --user=<your_username> --protocol=gp --usergroup=gateway https://wfh.vivint.com`.
    - If you are in the office, you can skip this step.
    - After the dotfiles install has finished, that `openconnect` command (with corresponding arguments) will be aliased 
    and you can simply run `vpn` from then on to connect to the VPN.
1. Open up `bash/aliases.symlink` (e.g. with `vi` because `vim` hasn't been installed yet) and you'll see an alias for 
the `vpn` command. Change the `--user` argument of that command to use your username rather than mine and save the file.
    - Your username is everything in your vivint email before the `@`.
1. `./scripts/generate_keys.sh`
    - You will be prompted for some passwords during this script. I usually leave my passwords empty for my SSH keys,
      but it's probably a good idea to use a real password for your tigerguard key.
    - This will generate the `~/.ssh` directory along with three keys: a general purpose RSA token, a general purpose
    ed25519 token and a tigerguard ed25519 token.
1. Add the ed25519 public key that was generated to both GitLab and Bitbucket so that you can clone, pull from,
and push to repos via SSH.

Now you should be able to run the installation script:

1. `cd ~/.dotfiles`
1. `./install.sh`
    - This is going to take a while and, unfortunately, it will probably prompt you for a few things (e.g. your `sudo` 
    password) so you'll need to stay somewhat nearby.
    - When it gets to the Rust installation, you'll need to select the default install.
1. Restart your computer (for some of the changes to take effect). Then, after it boots again:
1. `cd ~/.dotfiles`
1. `./finalize.sh`
    - This doesn't take nearly as long as the install script, but it will also prompt you for some information,
    particularly during the Google Cloud installation. When you start getting GCP prompts:
        - Choose "yes" that you want to login. This will pull up a browser.
        - Use your Vivint email to login.
        - Choose "Google Workspace Account"
        - You might have to go through some Okta authentication stuff at this point.
        - Allow access when you get prompted.
        - After you have allowed access, go back to the terminal and you should be prompted to select a project. Select 
        the `video-analytics-193323` project.
1. `source ~/.bash_profile`
1. Everything should now be installed. See below for a review of what is now available to you.

## What is installed?

### Shell Stuff

Various prompt customizations and aliases are installed with the dotfiles. For bash-specific items, see the contents of
the `bash` folder. Git items can be found in the `git` folder. Vim customizations can be found in the `vim` folder.

### Build and Language Tools

- The camera firmware is mostly a combination of C/C++ and Rust. Support for both of these languages is installed as part
of the dotfiles installation.  You can view and edit the list of package dependencies at `scripts/first/03_install_package_dependencies.sh`. Rust is installed via the `scripts/first/06_install_rustup.sh` script.
- AmbaUSB is a tool that is used to flash the various cameras with firmware images. It is also installed with the rest of
the dotfiles. You can run this tool either from the command line using the `ambausb` command or just look for it in the
Applications menu.
- There is a docker image that already has all the dependencies required to build camera firmware. The dotfiles
installation installs Docker itself and configures your user appropriately so you can start building camera firmware.
(Note that the first time you download the Docker image and build the camera firmware, it will take a LONG time.)
- Phil Burr has added some very nice tweaks to the camera firmware to ease in the development process. You can see that
the dotfiles install put a `.developer` folder in the root directory of your camera-build repo. This folder contains
files that will enable you to SSH into an unlocked camera without having to fuss with keys and gRPC requests and it also
enables serial access by default.

### Common Repositories

- The vast majority of the work you will do on the camera team is in the
[camera-build](https://gitlab.com/vivint/camera/camera-build) repo or its submodules, all hosted on GitLab. You can find 
this repo (with recursively cloned submodules) at `~/git/camera-build`.
- Many camera tickets also require corresponding panel work. The panel code is found in the [embedded-apps](https://source.vivint.com/projects/EM/repos/embedded-apps/browse)
repo hosted on Bitbucket. This repo is cloned locally at `~/git/embedded-apps`.

### Visual Studio Code

My preferred IDE is VSCode. It has a lot of great features and you can find a good extension for just about anything you
would want to do with your IDE. The extensions included as part of this install are:

- Python
- C/C++
- Rust
- Vim (I like to use vim keybindings when I develop)
- Nord (The color theme I prefer)

To add an extension to be installed as part of the dotfiles installation, simply download the .vsix file from the VSCode
Marketplace and add it to the `vscode/extensions` folder prior to running the installation.

### Panel Development Virtual Environment

The dotfiles install creates a virtual environment for panel development called "panel_proxies" using the 
[`virtualenvwrapper`](https://virtualenvwrapper.readthedocs.io/en/latest/) utility. To enter the panel virtual
environment, use the command `workon panel_proxies`. To exit the virtual environment, use the command `deactivate` from
within the virtual environment.

The `virtualenvwrapper` utility is pretty handy for creating and working with virtual environments so, if that's
something you do regularly, you should take a look.

### Google Cloud Utilities

There are some utilites such as `gcloud` and `gsutil` that enable us to retrieve artifacts from Google Cloud Storage,
which is where we store everything.

TODO: Probably could provide a better description, but I don't know much about this stuff.

### Slack

Slack is the app we use for messaging throughout the company. You can search for it and should easily find it.

### Spotify

The first time you want to open Spotify, you'll need to run the "lpf-spotify-client" (just search it on your machine).
After that has run, you'll see the Spotify application from now on.