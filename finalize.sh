#!/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

# Run all the finalization scripts.
for f in $SCRIPT_DIR/scripts/second/*; do
  /bin/sh $f
done

# Set up symbolic links to bashrc files.
bashrc_dir=$HOME/.bashrc.d
if [ ! -d "$bashrc_dir" ]; then
  mkdir -p $bashrc_dir
fi
chmod 775 $bashrc_dir

for f in $SCRIPT_DIR/bash/*; do
  if [ -f "$f" ]; then
    rc_name=`basename "$f"`
    ln -sf "$f" "$bashrc_dir/$rc_name"
  fi
done

# Set up a symbolic link to the gitconfig.
ln -sf $SCRIPT_DIR/git/gitconfig.symlink $HOME/.gitconfig

# Set up a symbolic link to the vimrc file.
ln -sf $SCRIPT_DIR/vim/vimrc.symlink $HOME/.vimrc

# Set up a symbolic link with correct permissions to the SSH config file.
ln -sf $SCRIPT_DIR/ssh/config $HOME/.ssh/config
chmod 644 $HOME/.ssh/config

# Set up symbolic links for vscode files.
ln -sf $SCRIPT_DIR/vscode/settings.json $HOME/.config/Code/User/settings.json
ln -sf $SCRIPT_DIR/vscode/keybindings.json $HOME/.config/Code/User/keybindings.json

# HACK ALERT: the google cloud script doesn't work the first time (seems like a timing thing). Running it again has
#             worked for me.
/bin/sh/ $SCRIPT_DIR/scripts/second/22_install_google_cloud_sdk.sh

printf "\n\nYou're all set! The final step is to run:\n\n"
printf "\tsource ~/.bash_profile\n\n"
